<!DOCTYPE HTML PUBLIC "-//W3O//DTD W3 HTML 2.0//EN">
<HTML><HEAD>
<TITLE> Meta-Level Techniques for Separating Application and Visualization</TITLE>
</HEAD>
<BODY>
<P>
 <BR> </P>
<HR>
<P><A NAME="tex2html92" HREF="node7.html"><IMG
ALIGN="BOTTOM" ALT="next" SRC="next_motif.gif"></A>  
<A NAME="tex2html90" HREF="paper.html"><IMG
ALIGN="BOTTOM" ALT="up" SRC="up_motif.gif"></A>  
<A NAME="tex2html84" HREF="node5.html"><IMG
ALIGN="BOTTOM" ALT="previous" SRC="previous_motif.gif"></A>         <BR>
<B> Next:</B> <A NAME="tex2html93" HREF="node7.html"> Status of Implementation</A>
<B>Up:</B> <A NAME="tex2html91" HREF="paper.html">No Title</A>
<B> Previous:</B> <A NAME="tex2html85" HREF="node5.html"> The Box Layout </A>
<BR> </P>
<HR>
<P>
</P>
<H1><A NAME="SECTION00060000000000000000"> Meta-Level Techniques for
Separating Application and Visualization</A></H1>
<P>
<A NAME="MOP">&#160;</A>
</P>
<P>
In order to ensure clear program structures, application and visualization
objects should be separated. The second part of this paper discusses how to use
the meta-object protocol of CLOS (see also elsewhere [<A
HREF="node11.html#KiczalesBobrow90">15</A>,<A HREF="node11.html#Kiczalesetal91">16</A>])
for linking application and visualization layers. This link is mostly based on
interesting events generated by application objects.  These events have to be
visualized in one way or another. The recognition of interesting events is a
well-known problem since these events are very often only indirectly reflected
by algorithms. We refer to Brown [<A HREF="node11.html#Brown88b">17</A>] for a
detailed discussion of these problems.
</P>
<P>
Our approach associates visualization objects with given application objects
without requiring any source code modifications to the application. We support
multiple views as well as controllers for manipulating the application's data
structures. Several other mechanisms have also been developed
(Model-View-Controller-Scheme [<A HREF="node11.html#GoldbergRobson83">18</A>],
CLUE [<A HREF="node11.html#KimbroughLaMotte89">19</A>], Presentation-Types [<A
HREF="node11.html#Symbolics88">20</A>,<A HREF="node11.html#CLIM90">21</A>]).
</P>
<P>
As example application we chose a simple constraint net. There is no need to
present the application code since everything can be found in detail elsewhere [<A
HREF="node11.html#WinstonHorn89">22</A>]. The application provides a simple
model of a stock exchange scenario. When are some stocks to split? The
participants have uncertain knowledge and are influenced by one another. A
constraint net models these influences by propagating certainty-estimation
intervals between 0 and 1. This interval of a `broker' might be visualized by a
gauge [<A HREF="node11.html#WinstonHorn89">22</A>]. The implementation
distinguishes assertion objects (brokers, mystics, virtual intermediates, etc.)
and constraint objects (or, and). Figure <A HREF="node6.html#constraints">9</A>
shows a snapshot of a program animation. The example configuration consists of
gauges for assertions and simple nodes for constraints.
</P>
<P>
</P>
<P><A NAME="1468">&#160;</A><A NAME="constraints">&#160;</A><IMG
ALIGN="BOTTOM" SRC="constraint-3.gif">
<BR><STRONG>Figure 9:</STRONG> The upper and lower bounds are indicated by
shaded rectangles [<A HREF="node11.html#WinstonHorn89">22</A>]. The gauges show
the estimation interval from 0 (bottom) to 1 (top).<BR>
</P>
<P>
</P>
<P>
The visualization in Figure <A HREF="node6.html#constraints">9</A> can be built
with the techniques described in the previous sections. The graph is defined by
a set of participants and a successor function
<TT>stock-exchange-wizard</TT>.
</P>
<P>
</P>
<PRE>(defmethod stock-exchange-wizard ((participant assertion))
  &quot;Wizard's information about connections of assertion objects.&quot;
  (assertion-constraints participant)) ; OR nodes

(defmethod stock-exchange-wizard ((participant constraint))
  &quot;Wizard's information about connections of constraint objects.&quot;
  (list (constraint-output participant))) ; brokers, mystics, etc.
</PRE>
<P>
</P>
<P>
Another function, <TT>application-visualization-coupler</TT>, is used to define
a mapping from application to visualization objects (see Section
<A HREF="node6.html#linkingapplicationandvisualiztion">5.2</A>). Both functions
are generic, i.e. different mappings may be specified for different classes of
application objects.
</P>
<P>
</P>
<H2><A NAME="SECTION00061000000000000000"> Indirect Values for Visualization
Objects</A></H2>
<P>
Visualization objects have to refer to objects of the application side. There
should exist a ``dynamic'' binding which could be easily maintained provided
that classes of visualization objects offer support for some kind of active
values [<A HREF="node11.html#BobrowStefik83">23</A>]. We present a simplified
CLOS metaclass supporting non-nested active values which we call <EM> indirect
values.</EM>  Indirect values are defined by the form <TT>
(make-indirect-object object reader writer)</TT> where  is optional. The
following definitions sketch an implementation using a new metaclass and a
corresponding meta-level method for the generic slot accessor function
<TT>slot-value-using-class</TT>. Writing to slots with indirect values can be
implemented analogously.
</P>
<P>
</P>
<PRE>(defclass indirect-slots-class (standard-class)
  ()
  (:documentation &quot;Metaclass supporting indirect slot values.&quot;))

(defmethod check-super-metaclass-compatibility ((x indirect-slots-class)
                                                (y standard-class))
  t) ; We do not care about that in this paper (see Graube [<A
HREF="node11.html#Graube89">24</A>])

(defmethod slot-value-using-class ((class indirect-slots-class) 
                                   object slot-descriptor)
  (let ((direct-slot-value (call-next-method)))     ; get slot value
    (if (indirectp direct-slot-value)               ; test for indirect value
      (funcall (indirect-reader direct-slot-value)  ; notify application object
               (indirect-object direct-slot-value)) 
      direct-slot-value)))
</PRE>
<P>
</P>
<P>
Visualization objects may have <TT>indirect-slots-class</TT> as their
metaclass. With indirect slot values every slot access is delegated to the
corresponding application object if required. Using the meta-object protocol it
would be easy to determine all indirect objects or that indirect object referred
to by a specific slot.  The gauges for the stock exchange example use this
metaclass to refer to the exchange participants.  But what about the other
direction: the gauges have to be ``informed'' when the participants' estimations
of stock splits change.
</P>
<P>
</P>
<H2><A NAME="SECTION00062000000000000000"> Slot Demons for Application Objects</A></H2>
<P>
<A NAME="linkingapplicationandvisualiztion">&#160;</A>
</P>
<P>
An assertion object has one slot for the lower and one for the upper bound
estimation. The corresponding visualization objects have to be informed when
either of these slot values change.  The most obvious way to achieve this is to
define the assertion class with a metaclass that allows <EM> demon functions</EM>
 to be attached to slots. The ``real'' value of a slot is a structure that
provides a value facet and an -modified facet [<A
HREF="node11.html#RobertsGoldstein77">25</A>]. All slot demon functions are
evaluated when the slot value changes. The implementation of slot demons is
similar to the one of indirect slot values. We introduce a metaclass <TT>demon-slots-class</TT>
and define modified versions of <TT>slot-value-using-class</TT> and <TT>(setf
slot-value-using-class)</TT>, which access the value facet. The latter one
evaluates the demons in the <TT>if-modified</TT> facet. Slot demons should be
made removable.
</P>
<P>
Thus, the function <TT>application-visualization-coupler</TT> mentioned above
can be defined as follows.
</P>
<P>
</P>
<PRE>(defmethod application-visualization-coupler ((participant assertion))
  (let ((assertion-gauge (make-two-level-gauge   ; with indirect values
                           (make-indirect-object participant 
                                                 assertion-lower-bound)
                           (make-indirect-object participant 
                                                 assertion-upper-bound))))
    (add-slot-if-modified-demon
      participant  ; object
      'lower-bound ; slot name
      #'(lambda    ; demon function
          (assertion-obj name-of-modified-slot old-value new-value)
          (gauge-update assertion-gauge)))
    (add-slot-if-modified-demon
      participant  ; object
      'upper-bound ; slot name
      #'(lambda    ; demon function
          (assertion-obj name-of-modified-slot old-value new-value)
          (gauge-update assertion-gauge)))
    assertion-gauge))

(defmethod application-visualization-coupler ((participant or-box))
  (make-node &quot;OR&quot;))
</PRE>
<P>
</P>
<P>
Demon functions are closures which provide access to the corresponding
visualization object. The gauges for assertion objects (broker, etc.) use
indirect values to access assertion objects. The objects representing
constraints as nodes are the same as in the class browser example.
</P>
<P>
</P>
<H2><A NAME="SECTION00063000000000000000"> Method Demons</A></H2>
<P>
Slot demons offer an elegant way of defining slot accesses as interesting events
and hence updating corresponding visualization objects. Not only slot accesses
are subject to updating a visualization.  Every method might define an event of
interest. Slot accesses are only special cases. General <EM> method demons</EM>
 can be implemented using the meta-object protocol of CLOS. The idea is to wrap
a method with a so-called <EM> wrapper method</EM>  which has slots to refer to
both the demon functions and the original method (see Figure
<A HREF="node6.html#wrapper">10</A>). When all demons are removed the wrapper
method itself is removed, too. In this case there is no overhead as with a
metaclass which provides own methods for slots accesses  (e.g. for indirect
values) that overwrite the standard slot accessor methods.
</P>
<P>
</P>
<P><A NAME="1520">&#160;</A><A NAME="wrapper">&#160;</A><IMG
ALIGN="BOTTOM" SRC="wrapper.gif">
<BR><STRONG>Figure 10:</STRONG> Outline of a wrapper method.<BR>
</P>
<P>
</P>
<P>
A major disadvantage of this wrapping slot accessor is that demons are evaluated
for all instances, i.e. they are slot but not instance-specific. Method demons
do not solve the problem of compound slot accesses, either. In the following two
subsections we propose a solution to these problems.
</P>
<P>
</P>
<H2><A NAME="SECTION00064000000000000000"> Instance-Specific Meta-Objects</A></H2>
<P>
The CLOS meta-object system assigns to metaclasses the responsibility for both
structure (implementation) and behavior of instances. There are other meta-level
systems which distinguish between structural and computational meta-objects [<A
HREF="node11.html#Ferber89">26</A>]. In this section we present ideas to
provide some kind of dynamic meta-level influence in CLOS [<A
HREF="node11.html#Cunis90">27</A>]. We implement meta-objects as instances of a
class <TT>standard-meta-object</TT> which serves not as a CLOS metaclass. The
standard slot access protocol, which uses the method
<TT>slot-value-using-class</TT>, is analogously extended for these ``simple''
meta-objects.
</P>
<P>
</P>
<PRE>(defclass standard-meta-object ()
  ()
  (:documentation 
   &quot;Class of all meta-objects that provide instance-specific meta behavior.&quot;))

(defmethod slot-value-using-meta-object ((mobj standard-meta-object)
                                         object slot-descriptor)
  (call-next-meta-method))

(defmethod (setf slot-value-using-meta-object) ((mobj standard-meta-object)
                                                object slot-descriptor)
  (call-next-meta-method))
</PRE>
<P>
</P>
<P>
Meta-objects can be assigned to instances with metaclass
<TT>extensible-standard-class</TT>.  This metaclass describes classes with
instances that have one additional or implicit slot called
<TT>meta-objects</TT> (see Figure <A HREF="node6.html#metaobjects">11</A>). A
set of meta-objects can be assigned to this slot.
</P>
<P>
</P>
<P><A NAME="1574">&#160;</A><A NAME="metaobjects">&#160;</A><IMG
ALIGN="BOTTOM" SRC="meta-objects.gif">
<BR><STRONG>Figure:</STRONG> Meta-objects for instances with metaclass
-standard-class.<BR>
</P>
<P>
</P>
<P>
An example method handling slot accesses is defined as follows.
</P>
<P>
</P>
<PRE>(defmethod slot-value-using-class ((class extensible-standard-class)
                                   object slot-descriptor)
  (let ((slot-name (slotd-name slot-descriptor)))
    (if (eq slot-name 'meta-objects) ; prevent recursive slot access
      (if (slot-boundp object 'meta-objects)
        (call-next-method)
        nil)
      (let ((*meta-objects* (slot-value object 'meta-objects))
            (*meta-class-generic-function* #'(lambda () (call-next-method)))
            (*meta-object-generic-function*
              #'(lambda (meta-object)
                  (slot-value-using-meta-object meta-object
                                                object
                                                slot-descriptor))))
       (declare (special *meta-objects*
                         *meta-object-generic-function*
                         *meta-class-generic-function*))
       (if (null *meta-objects*)
         (call-next-method)
         (call-next-meta-method))))))
</PRE>
<P>
</P>
<P>
The function <TT>call-next-meta-method</TT> is comparable to the function <TT>call-next-method</TT>.
It evaluates
<TT>slot-value-using-meta-object</TT> for the ``next'' meta-object in the list
of meta-objects (see Figure <A HREF="node6.html#metaobjects">11</A>). The
default behavior of <TT>slot-value-using-meta-object</TT> is to evaluate
<TT>call-next-meta-method</TT> again (s.a.). This default behavior may be
augmented or overwritten by subclasses of <TT>standard-meta-object</TT> (s.b.).
 When there are no meta-objects (left),
<TT>call-next-meta-method</TT> invokes the ``normal'' slot access functionality
of <TT>standard-class</TT>. There is some additional code needed to enable
passing of different parameters to the next metamethod just as with <TT>call-next-method</TT>.
</P>
<P>
</P>
<PRE>(defun call-next-meta-method ()
  (declare (special *meta-objects*
                    *meta-object-generic-function*
                    *meta-class-generic-function*))
  (if (endp *meta-objects*)
    (funcall *meta-class-generic-function*)
    (funcall *meta-object-generic-function* (pop *meta-objects*))))
</PRE>
<P>
</P>
<P>
We use these meta-level techniques to extend our constraint example. Using the
protocol described above visualizations of particular instances can be provided
with little programming effort. For instance, a meta-object could be defined by
a class
<TT>visualizer-meta-object</TT>. This class combines a visualization object
with a list of interesting slots. Every writing access to these slots is
followed by calling the instance-specific visualization object.
</P>
<P>
</P>
<PRE>(defclass visualizer-meta-object (standard-meta-object)
  ((visualizer :initarg :visualizer
               :accessor visualizer
               :initform #'(lambda (\&amp;rest ignore) nil))
  (interesting-slots :initarg :interesting-slots
                     :reader interesting-slots))
  (:default-initargs :interesting-slots nil))

(defmethod (setf slot-value-using-meta-object) :after (new-value
                                                       (mobj visualizer-meta-object)
                                                       object slot-descriptor)
  (if (member slot-name (interesting-slots mobj))
    (funcall (visualizer mobj) object (slotd-name slot-descriptor))))
</PRE>
<P>

</P>
<P>
Be <TT>your-opinion</TT> the assertion object of our constraint example (see
Figure <A HREF="node6.html#constraints">9</A>).  We add only to this object a
corresponding meta-object which prints <TT>your-opinion</TT>'s decision about
buying stocks. This behavior can be easily reverted by removing this meta-object
from the implicit slot <TT>meta-objects</TT>.
</P>
<P>
</P>
<PRE>(add-meta-object your-opinion
                 (make-instance 'visualizer-meta-object
                                :interesting-slots '(lower-bound upper-bound)
                                :visualizer
                                  #'(lambda (assertion slot-name)
                                      (if (&gt; (assertion-lower-bound assertion) 0.75)
                                        (print 'buy) ; or any other visual feedback
                                        (print 'donot-buy)))))
</PRE>
<P>
</P>
<P>
Another behavior might be to temporarily modify a reading access to a slot
value. After adding a meta-object of class
<TT>buying-indicator-meta-object</TT> to the object <TT>your-opinion</TT>,
each reading access to the slot <TT>lower-bound</TT> of
<TT>your-opinion</TT> returns the slot value and a buying indicator.
</P>
<P>
</P>
<PRE>(defclass buying-indicator-meta-object (standard-meta-object)
  ())

(defmethod slot-value-using-meta-object ((mobj buying-indicator-meta-object)
                                         object slot-name)
  (if (eq slot-name 'lower-bound)
    (let ((slot-value (call-next-meta-method)))
      (if (&gt; slot-value 0.75)
        (values slot-value 'buy)
        (values slot-value 'donot-buy)))
    (call-next-meta-method)))

(add-meta-object your-opinion (make-instance 'buying-indicator-meta-object))
</PRE>
<P>
</P>
<P>
Instance-specific meta-objects have also been proposed by Maes [<A
HREF="node11.html#Maes87">28</A>,<A HREF="node11.html#MaesNardi88">29</A>].
The difference to our approach is that (at least basically) only one meta-object
may be assigned to an object at a certain time. One may of course argue that our
CLOS implementation is a little impure because of using different mechanisms:
metaclasses and meta-objects. Moreover, not all meta-objects may be compatible
(see Graube [<A HREF="node11.html#Graube89">24</A>] for a discussion). There
also remains some overhead even when no meta-objects are attached to an
instance.
</P>
<P>
</P>
<H2><A NAME="SECTION00065000000000000000"> Compound Events</A></H2>
<P>
<A NAME="events">&#160;</A>
</P>
<P>
The events of interest mentioned in the previous sections are only defined
implicitly, i.e. there are no objects generated to describe events.  However, an
explicit representation of event conditions is needed to handle more complicated
(compound) events. For instance, the situation in our stock example ``all
brokers have a lower bound estimation greater than
<IMG ALIGN="BOTTOM" ALT="" SRC="img53.gif">'' might be defined as a compound
event. Then, a coordination problem arises since several objects are now
concerned. It should even be possible to combine several events (e.g. value
changes of different slots in different objects) as a compound event.
</P>
<P>
We need a declarative specification of such event conditions and a management
system responsible for collecting and monitoring announcements of subevents
rather than simple demons handling slot accesses directly (see Figure
<A HREF="node6.html#manager">12</A>).
</P>
<P>
</P>
<P><A NAME="1639">&#160;</A><A NAME="manager">&#160;</A><IMG
ALIGN="BOTTOM" SRC="manager.gif">
<BR><STRONG>Figure:</STRONG> Instances <TT>i-1</TT> to <TT>i-4</TT> with
slots
<TT>s-1</TT> to <TT>s-3</TT> report new values to an event manager. The agenda
is extended at the bottom.<BR>
</P>
<P>
</P>
<P>
Subevents provide a data structure (in Figure <A HREF="node6.html#manager">12</A>
instances of these structures are represented by black dots) for storing event
information (e.g. an object, its changed slot, the previous value, and the new
value). The management system uses an agenda to access incoming subevents. The
supervisor decides when agenda entries can be compiled to a compound event.
</P>
<P>
The main problem with compound events is the necessity to define the notion of a
``step.'' The definition of a step depends on an ``interpretation'' of the
object system, e.g. different step interpretations might exist.
</P>
<P>
Meta-level programming can be used to define a certain step interpretation for
an existing object system. We distinguish two kinds of steps: object steps and
system steps. Let's have a look at the constraint example again. We assume that
accesses to the slots
<TT>lower-bound</TT> and <TT>upper-bound</TT> are recorded on the agenda. An
entry of the agenda comprises the object together with the corresponding slot
values of <TT>lower-bound</TT> and
<TT>upper-bound</TT>.
</P>
<P>
An object step is induced when there already exists a slot access entry for a
certain slot and the meta-system attempts to add another entry for this slot. If
there is an entry for the other slot, the system combines the entries to an
object step using the values of the current entry. If there is no such slot
entry for the ``other'' slot, the current slot values of the object are used to
determine its value. Afterwards, all used agenda entries of the corresponding
object are deleted. The new entry is inserted.
</P>
<P>
System steps are defined analogously, just one level higher. A system steps
consists of a list of objects with a sequence of slot values associated to them.
As an example we consider two brokers as a (trivial) object ``system.'' A system
step is induced when there already exists an object step for one of the brokers
and an attempt is made to create another object step just for the same broker
object. If no step for the other broker has been recorded, its current slot
values are copied and used for the system step. System steps, with the slot
values recorded for each object, can be supplied to a visualization component
for further processing.
</P>
<P>
We emphasize that this is only one possible proposal for an event manager,
though it is suitable for our stock exchange example.
</P>
<P>
Other visualization systems (e.g. see in Linden [<A HREF="node11.html#Linden90">30</A>])
record the whole agenda (or the stream of events) on a file so that a postmortem
visualization can be provided (tractable only for `small' systems).
</P>
<P>
<BR> </P>
<HR>
<P><A NAME="tex2html92" HREF="node7.html"><IMG
ALIGN="BOTTOM" ALT="next" SRC="next_motif.gif"></A>  
<A NAME="tex2html90" HREF="paper.html"><IMG
ALIGN="BOTTOM" ALT="up" SRC="up_motif.gif"></A>  
<A NAME="tex2html84" HREF="node5.html"><IMG
ALIGN="BOTTOM" ALT="previous" SRC="previous_motif.gif"></A>         <BR>
<B> Next:</B> <A NAME="tex2html93" HREF="node7.html"> Status of Implementation</A>
<B>Up:</B> <A NAME="tex2html91" HREF="paper.html">No Title</A>
<B> Previous:</B> <A NAME="tex2html85" HREF="node5.html"> The Box Layout </A>
<BR> </P>
<HR>
<P>
<BR> </P>
<HR>
<P></P>
<ADDRESS>
<I>Volker Haarslev <BR>
Tue Jun 11 13:34:48 MET DST 1996</I>
</ADDRESS>
</BODY>
</HTML>
