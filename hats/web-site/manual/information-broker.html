<!DOCTYPE html PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
          
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          
  <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
          
  <meta name="GENERATOR" content="Mozilla/4.75C-CCK-MCD {C-UDP; EBM-APPLE} (Macintosh; U; PPC) [Netscape]">
  <title>The Hats Simulator Manual : The Information Broker</title>
</head>


<style>

A:active { color: "#2135ee"; text-decoration : none; }  
A:link { color: "#2135ee"; text-decoration : none; } 
A:visited { color: "#ff172a"; text-decoration : none; }  
A:hover { color: "#4682B4" }

</style>


<!- <body text="#000000" bgcolor="#ffffff" link="#2135ee" vlink="#ff172a" alink="#2135ee">

<body text="#000000">

[<a href="index.html">index</a> | <a href="interface.html">Interface</a> | <a href="scoring.html">Scoring</a>] <br> <br>

<b><font size="+2">Information Request Framework: The <em>Information Broker</em></font></b>

<blockquote>
<a href="#cost">The cost of information</a><br>
<a href="#noise">Noise model</a>
</blockquote>

<blockquote>
One of the goals of the Hats Simulator project is to provide an experimental environment in which to study the economics of the value of information when attempting to identify and disrupt malevolent behavior.  Information about the state of the Hats simulator is costly.  The quality of the information obtained is determined by the amount paid.  The following two sections describe the two central components to the request framework: the cost of information and noise.
</blockquote>

<hr width="100%" size="2" align="Left">

<b><a name="cost"><font size="+1">The cost of information</font></a></b><br><br>

<blockquote>
Payment specified via the <tt>:payment</tt> keyword argument of IB Request functions determines the base accuracy (as a probability) of the information to be returned.  In the current implementation, increased accuracy requires exponentially more "algorithmic dollars."  The payment function, shown in the following equation, maps payment to probability.
</blockquote>

<center><img width="300" src="figures/payment-probability.pdf"></center>

<blockquote>
The following graph plots probability over a range of payments, following the equation:
</blockquote>

<center><img src="figures/pay->prob-graph.pdf"></center>

<blockquote>
The same function is applied to every payment-based request.  The payment must be a positive real number, otherwise an error is returned.
</blockquote>

<hr width="100%" size="2" align="Left">

<b><a name="noise"><font size="+1">Noise model</font></a></b><br><br>

<blockquote>
The development of a suitable noise model and the schemes for how noise is applied to requested information is, itself, an entire field of study.  Three approaches are presented in increasing order of complexity:

<ul>
	<li>The analyst may only request a particular piece of information once and must choose the level of payment for (and therefore quality of) the information at the time of request.  No additional requests may be made.  The analyst must decide by the time of request the value of that piece of information.</li>
	<li>The analyst may request information multiple times.  However, in order to receive information beyond previous request(s), the analyst must pay more than previous requests (according to the payment scale).  Repeated requests at or below the same level will return precisely the same information, but paying more returns less noisy versions of the original request.</li>
	<li>The analyst may request information multiple times, paying varying amounts.  This approximates the existence of multiple information sources (for example, acquiring information from multiple witnesses of an event).  Such multiple information sources might be made explicit, introducing the potential of modeling sources of trust relationships.</li>
</ul>

Many other schemes are possible, but these provide some indication of the wide variety of approaches to noise application.  
</blockquote>

<blockquote>
In keeping with our approach of starting simple, the current implementation of the information broker employs the first scheme.  The payment the analyst specifies determines the base probability <em>p</em> of whether, and to what degree, the information requested will be noisy: with probability <em>p</em>, the information requested is returned in its entirety, otherwise the noise model is applied.
</blockquote>

<blockquote>
Although this basic noise application scheme is simple, there remain a variety of different types of information that may be requested and each requires a different noise model variant.  The table below summarizes how different types of requested information are made noisy. 
</blockquote>

<center><img width="90%" src="figures/noise-model.pdf"></center>

<blockquote>
Following this noise application scheme, analysts may only request each piece of information <em>once</em>.  Some information, such as the capabilities currently carried by a hat (<tt>ib-hat-capabilities</tt>), is updated at each tick, so the analyst may request that information once each tick.  Other information does not update, such as information about the members of a meeting that took place (<tt>ib-meeting-participants</tt>) - here the analyst is allowed only <em>one</em> request of this information.  The column labeled "Request Frequency" shows the frequency with which an analyst may request information. (To clarify: a request frequency of 1 refers to the particular <em>arguments</em> used in specifying the information requested (e.g., the particular meeting time and location))
</blockquote>

<blockquote>
The table is split into two groups based on whether the requested information is a single element (bottom portion of the table) or a list of elements (top portion of the table).
</blockquote>

<blockquote>
<font size="+1"><b>Lists</b></font><br>

Noise is applied to lists in two stages: first, noise affects the length of the list to be returned, and then noise is applied to each element of the list.  The two main columns on the right-hand side of the list portion of the table indicate how noise is applied to list-length and to each element; in either case, noise is applied differently depending on whether or not the request is for information about entities that exist or events that occurred - true, non-noisy information about entities that do not exist or events that did not occur is returned as <tt>NIL</tt>.  
</blockquote>

<blockquote>
List length is determined by sampling a random value from a normal distribution with a standard deviation of 1.0 and a variable mean.  (The sampled value is rounded to make it a valid list length.)  The "Mean List Length" column describes how the mean for the normal sampling distribution is set.  For example, if the analyst requests the current contents of a Hats world location (using <tt>ib-location-contents</tt>), and there are in fact 3 hats at that location, then the length of the potential return information (3) determines the mean; subsequently, the noisy length of the list of hats that will be returned as a result of the request will be a random number selected from a normal distribution with mean 3, standard deviation 1.  If, on the other hand, no hats exist at that location, then the mean of the normal distribution is 2.  These means have been chosen because they result in reasonable values relative to the current default parameter manager.  If the analyst requests information involving a list and the selected random value rounds to 0 or lower, then the return value will be an empty list (or <tt>NIL</tt>).
</blockquote>

<blockquote>
Next, assignments are made for each element slot in the list to be returned.  For each element, the noise model again uses the base probability <em>p</em> to determine whether the element slot will be noisy.  If it is to be noisy, an element of the requested information type is uniformly randomly selected (with replacement) for the set of <em>all</em> elements of that type.  For example, a random hat would be selected from <em>all</em> existing hats.  In the case of trades, a noisy trade consists of two randomly chosen hats and one randomly chosen capability.  With probability 1-<em>p</em> the element will <em>not</em> be noisy.  In this case, the element will be uniformly selected, <em>without</em> replacement, from the list of elements that would be returned if the information was uncorrupted; if the request is for information that does not exist, then that element of the list will be empty (<tt>NIL</tt>).
</blockquote>

<blockquote>
It is possible for the noisy returned list to have a greater length than the length of the uncorrupted list of elements that would be returned if no noise was applied.  In this case, it is possible for each of these uncorrupted elements to be selected -- each additional non-noisy selection will be empty, reflecting the fact that there is no more information available.  For example, suppose three hats <tt>(H1, H2, H3)</tt> are at location <tt>(3,4)</tt>, and an analyst requests information of who is at that location.  (This request might look like: <tt>(ib-location-contents :x-location 3 :y-location 4 :payment 200)</tt>.)  The Information Broker determines that the returned information will be noisy, and a return list of length 5 is chosen.  When filling in the elements of the return list, the Information Broker happens to determine that the first four will be non-noisy, and the last element is randomly selected (the Broker chooses <tt>H23</tt>).  The returned list is now: <tt>(H1, H2, H3, NIL, H23)</tt>.
</blockquote>

<blockquote>
After any return list has been filled, any <tt>NIL</tt> entries and any duplicates are removed -- in the example of the previous paragraph, the final return list is <tt>(H1, H2, H3, H23)</tt>.
</blockquote>

<blockquote>
Selection of non-noisy information for <tt>ib-meeting-times</tt> is different than the other list types.  As the simulator advances, hats will tend to participate in more and more meetings.  A history is kept of when each hat participates in a meeting, and <tt>ib-meeting-times</tt> requests this list.  Only a portion of the most recent meetings is available for requests, whether noisy or not: even when the entire return information has been determined to be non-noisy, the length of the return list is still randomly chosen.  When noise is applied, the noise model works essentially like the rest, except that when filling the return list with a <tt>non-noisy</tt> element, the information broker selects meeting times in order of recency (starting with the most recent, then then the next-most recent, etc.).  As above, if all of the actual meeting times have been selected for return, subsequent non-noisy selections will be <tt>NIL</tt>.  A noisy meeting time is a random integer selected from the range between the current tick and the start time.
</blockquote>

<blockquote>
<font size="+1"><b>Elements</b></font><br>

The elements portion of the table describes noise applied to return information consisting of single elements.  In the case of <tt>ib-hat-obituary</tt>, if noise is applied, then the following scheme is used:
<br><br>
&nbsp &nbsp If the hat is inactive (dead)<br>
&nbsp &nbsp &nbsp &nbsp then <tt>return NIL</tt><br>
&nbsp &nbsp &nbsp &nbsp else <tt> return </tt> the difference between the current tick <br>
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp and a value randomly selected from an exponential distribution<br>
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp with rate <em>lambda</em> = 0.2 <br>
&nbsp &nbsp &nbsp &nbsp &nbsp Unless this value is 0 - then <tt>return NIL</tt> <br><br>

This method is employed even when the requested hat doesn't exist.
</blockquote>

<blockquote>
Random locations are selected when noise is applied to location information.  A random location is chosen by selecting two random numbers, one for each coordinate component (x, y).  The random numbers are selected from a standard normal distribution (mean 0, standard deviation 1.0).  The value selected is then multiplied by the entire range of the x or y axis of the Hats World game board and divided by 10.  This heuristic returns reasonable distances relative to the size of the game board dimensions.  The adjusted value is then added to the true coordinate component.  If the adjusted coordinates exceed the boarders of the game board, the amount exceeded is "reflected".  For example, if a hat is at x-coordinate 3 and the adjustment is -5, then rather than returns an x-value of -2, the value is "reflected" to x = 2.  If, on the other hand, the Game World maximum x size is 10 and the adjusted value is 12, then the value is "reflected" to x = 8.
</blockquote>

<hr width="100%" size="2" align="Left">

[<a href="index.html">index</a> | <a href="interface.html">Interface</a> | <a href="scoring.html">Scoring</a>] <br> <br>
                                        
</body>
</html>
