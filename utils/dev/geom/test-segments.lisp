;;;-*- Mode: Lisp; Package: EKSL-UTILITIES -*-#| simple-header$Id: test-segments.lisp,v 1.2 2003/10/20 16:04:59 gwking Exp $Copyright 1992 - 2003 Experimental Knowledge Systems Lab, University of Massachusetts Amherst MA, 01003-4610Professor Paul Cohen, DirectorAuthor: Gary KingDISCUSSION|#(in-package u)(deftestsuite test-segment (test-geom) ())(deftestsuite test-segment-difference (test-segment)  ((main-segment     (segment (new-point 0.0 3.0) (new-point 5.0 8.0)))   (overlapping-and-shared-left-segment    (segment (new-point 4.0 7.0) (new-point 0.0 3.0)))   (overlapping-and-shared-right-segment    (segment (new-point 2.0 5.0) (new-point 5.0 8.0)))   (overlapping-and-fully-contained     (segment (new-point 1.0 4.0) (new-point 4.0 7.0)))   (partially-overlapping-segment     (segment (new-point 3.0 6.0) (new-point 8.0 11.0)))))(addtest (test-segment-difference)  (ensure    (samep     (segment (new-point 4.00 7.00) (new-point 5.00 8.00))    (segment-difference      main-segment overlapping-and-shared-left-segment))))